
def decimal_to_binary(num):
    binary_num = ''
    if(num == 0):
        return "0b0"
    if (num < 0):
        return "Positive Number Only"
    while(num >= 1):
        quotient = num //2
        remainder = num % 2
        binary_num = str(remainder) + binary_num
        num = quotient
    return '0b' + binary_num


print(decimal_to_binary(-4))