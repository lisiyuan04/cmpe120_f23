print("hello world")

def to_binary(number: int) -> str:
    """
    This is a comment
    Input: Number (integer)
    Output: String
    Example: Input = 15, Output = "0b1111"
    """
    if (number == 0): #If it is zero, it would return this instead of 0b with an empty string
        return "0b0"
    if (number < 0): #If it is negative
        return "Please enter a positive number"
    
    answer = ""
    
    # loop while >= 1
    while number >=1: 
        # Integer divide using the // operator
        quotient = number // 2
        # Get the remainder using the % operator
        remainder = number % 2
        
        # Accumulate result
        answer = str(remainder) + answer

        # Set the number we need to use for next time
        number = quotient

    return "0b" + answer
        
print(to_binary(15)) #should be 0b1111
print(to_binary(-1)) #should send a message
print(to_binary(0)) #should be 0b0
