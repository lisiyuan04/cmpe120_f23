#print("Hello World")
def decimal_to_binary(num):
    binary_num = ''
    if (num <= 0):
        return '0b0'
    while (num >= 1):
        quotient = num // 2 #floor integer division to take the truncated value
        remainder = num % 2 #takes remainder that will be added to binary resultant number
        binary_num = str(remainder) + binary_num #add the new remainder to the front of the existing binary num

        num = quotient #re-assign num to quotient so we can stop once quotient is less than terminating case
    
    return '0b' + binary_num #return number in binary format

result = decimal_to_binary(15)
print(result)