def dec_to_bin(num):
    bin_str = ''
    while num >= 1:
        #Keep dividing num by two until it is less than 1
        quo = num // 2
        print(quo, end=" ")
        remainder = num % 2
        print(remainder)
        num = quo
        bin_str = str(remainder) + bin_str     
    
    return bin_str

print(dec_to_bin(100))
